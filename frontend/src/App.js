import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.css';
import Navbar from "./components/Navbar";
import Dashboard from "./components/Dashboard";
import ProjectDetails from "./components/ProjectDetails";
import SignIn from "./components/SignIn";
import SignUp from "./components/SignUp";
import CreateProject from "./components/CreateProject";

class App extends React.Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />
          <Switch>
            <Route exact path="/" component={Dashboard} />
            <Route path="/project/:id" component={ProjectDetails} />
            <Route path="/signin" component={SignIn} />
            <Route path="/signup" component={SignUp} />
            <Route path="/create-project" component={CreateProject} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
