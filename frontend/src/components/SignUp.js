import React, { Component } from 'react'

class SignUp extends Component {
    state = {
        email: '',
        password: '',
        firstname: '',
        lastname: '',
    }
    handleValueChanges = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
    }
    render() {
        return (
            <div className="container">
                <form className="white" onSubmit={this.handleSubmit}>
                    <h5 className="grey-text text-darken-3">SignUp</h5>
                    <div className="input-field">
                        <label htmlFor="email"></label>
                        <input type="email" id="email" onChange={this.handleValueChanges} />
                    </div>
                    <div className="input-field">
                        <label htmlFor="password"></label>
                        <input type="password" id="password" onChange={this.handleValueChanges} />
                    </div>
                    <div className="input-field">
                        <label htmlFor="firstname"></label>
                        <input type="text" id="firstname" onChange={this.handleValueChanges} />
                    </div>
                    <div className="input-field">
                        <label htmlFor="lastname"></label>
                        <input type="text" id="lastname" onChange={this.handleValueChanges} />
                    </div>
                    <div className="input-field">
                        <button className="btn pink lightn-1 z-depth-0">SignUp</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default SignUp
