import React, { Component } from 'react'

class CreateProject extends Component {
    state = {
        title: '',
        content: ''
    }
    handleValueChanges = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
    }
    render() {
        return (
            <div className="container">
                <form className="white" onSubmit={this.handleSubmit}>
                    <h5 className="grey-text text-darken-3">Create Project</h5>
                    <div className="input-field">
                        <label htmlFor="title"></label>
                        <input type="text" id="title" onChange={this.handleValueChanges} />
                    </div>
                    <div className="input-field">
                        <label htmlFor="content"></label>
                        <textarea id="content" className="materialize-textarea" onChange={this.handleValueChanges}></textarea>
                    </div>
                    <div className="input-field">
                        <button className="btn pink lightn-1 z-depth-0">Create a Project</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default CreateProject
