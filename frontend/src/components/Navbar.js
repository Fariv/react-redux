import React from "react";
import { Link } from "react-router-dom";
import SignedInLinks from "./SignedInLinks";
import SignedOutLinks from "./SignedOutLinks";

const navbar = () => {
    return (
        <nav className="nav nav-wrapper grey darken-3">
            <div className="container">
                <Link to="/" className="brand-logo">Project Planner</Link>
                <SignedInLinks />
                <SignedOutLinks />
            </div>
        </nav>
    )
}

export default navbar;