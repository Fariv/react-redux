import React from "react";

class ProjectSummary extends React.Component {
    render () {
        return (
            <div className="card z-depth-0 project-summary">
                <div className="card-content grey-text text-darken-3">
                    <span className="card-title">
                        Project Title
                    </span>
                    <p>Posted By Ashraful Ferdous</p>
                    <p className="grey-text">3rd September 2019, 2AM</p>
                </div>
            </div>
        )
    }
}

export default ProjectSummary;